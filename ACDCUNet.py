import configparser
import os
import torch
import monai
import numpy as np
import nibabel as nib


class ACDCUNet:
    def __init__(self, fileName=None):
        self.model = monai.networks.nets.UNet(
            spatial_dims=2,
            in_channels=3,
            out_channels=3,
            channels=(8, 16, 32, 64, 128),
            strides=(2, 2, 2, 2),
            num_res_units=2,
        )
        self.loss_function = monai.losses.DiceLoss(sigmoid=True, batch=True)
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=1e-3)

        if fileName:
            self.model.load_state_dict(torch.load(fileName))


def build_dict_images(data_path, mode="training"):
    """
    Args:
        data_path (str): path to the root folder of the database
        mode (str): subset used; training or testing
    """

    # Test if mode is correct
    if mode not in ["training", "testing"]:
        raise ValueError(f"Please choose a mode in ['training', 'testing']. Current mode is {mode}.")

    # Define empty dictionary
    dicts = []

    parser = configparser.ConfigParser()

    for fileName in os.listdir(os.path.join(data_path, mode)):
        if fileName.startswith("patient"):
            with open(os.path.join(data_path, mode, fileName, "Info.cfg")) as stream:
                parser.read_string("[top]\n" + stream.read())
            cfg = dict(parser.items("top"))
            first_file = cfg["ed"].zfill(2)
            last_file = cfg["es"].zfill(2)

            patient_files = []
            for file in [first_file, last_file]:
                image_path = os.path.join(data_path, mode, fileName, f"{fileName}_frame{file}.nii.gz")
                label_path = os.path.join(data_path, mode, fileName, f"{fileName}_frame{file}_gt.nii.gz")

                if os.path.exists(label_path):
                    patient_files.append({"image": image_path, "label": label_path, "first_file": file == "01"})
            dicts.append(patient_files)

    return np.array(dicts)


# Load .gz files and split them in 2D images split into three classes.
class LoadImageData:
    def __init__(self, training=False):
        self.training = training
    
    def __call__(self, sample):
        image_path = sample["image"]
        img_nifti = nib.load(image_path)
        image = img_nifti.get_fdata()
        image = np.array(image, dtype=np.float32)

        label_path = sample["label"]
        label = nib.load(label_path).get_fdata()
        label = np.array(label, dtype=np.float32)
        right_ventricle = np.where(label == 1, 1, 0)
        myo = np.where(label == 2, 1, 0)
        left_ventricle = np.where(label == 3, 1, 0)

        images = [
            {
                "image": image[:, :, i],
                "label": label[:, :, i],
                "left_ventricle": left_ventricle[:, :, i],
                "myo": myo[:, :, i],
                "right_ventricle": right_ventricle[:, :, i],
                "image_meta_dict": {
                    "affine": np.eye(2),
                    "original_size": image.shape[:2],
                    "original_header": img_nifti.header if not self.training else 0,
                    "original_affine": img_nifti.affine if not self.training else 0,
                },
                "label_meta_dict": {"affine": np.eye(2)},
            }
            for i in range(image.shape[2])
        ]
        return images
