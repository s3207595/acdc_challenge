# Heart segmentation

This directory contains files to perform MRI heart segmentation in ACDC dataset.


![heart](pictures/sample_viz.png)


## Setting up directory

Download the ACDC dataset and put in the "Dataset" folder

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the requirements.

```bash
pip install -r requirements.txt
```